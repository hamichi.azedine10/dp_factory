http://www.w3sdesign.com/

The intent of the Factory Method design pattern is to:
"Define an interface for creating an object, but let subclasses decide which class
to instantiate. Factory Method lets a class defer instantiation to sub­classes." [GoF]

The Factory Method design pattern solves problems like:
How can an object be created so that subclasses can redefine which class to instantiate?
 How can a class defer instantiation to subclasses?
 
 The Factory Method design pattern provides a solution:
 Define a separate operation (factory method) for creating an object.
  Create an object by calling a factory method.

