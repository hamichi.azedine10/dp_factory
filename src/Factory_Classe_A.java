
public class Factory_Classe_A {
    String critere=null;

    public String getCritere() {
        return critere;
    }

    public void setCritere(String critere) {
        this.critere = critere;
    }

    public Class_A getClass_A() {

        if (critere == null) {
            return null;
        }else if (critere.equalsIgnoreCase("A1")){
            return new Class_A1();
        }
        else if (critere.equalsIgnoreCase("A2")){
            return new Class_A2();
        }else if (critere.equalsIgnoreCase("A3")){
            return new Class_A3();
        }
        else return null;
    }
}