public class program {


    public static void main(String[] args) {
        // DP name : Factory Method  - Category : creation  - scope : class (inheritance)
        //The intent of the Factory Method design pattern is to:
        //"Define an interface for creating an object, but let subclasses decide which class
        //to instantiate. Factory Method lets a class defer instantiation to subclasses." [GoF]

        //The Factory Method design pattern solves problems like:
        //How can an object be created so that subclasses can redefine which class to instantiate?
        //How can a class defer instantiation to subclasses?
        // more information see : http://www.w3sdesign.com/GoF_Design_Patterns_Reference0100.pdf

        Factory_Classe_A fabrique = new Factory_Classe_A();
        fabrique.setCritere("a3");
        Class_A  obj = fabrique.getClass_A();
        fabrique.setCritere("a1");
        obj=fabrique.getClass_A();

        if (obj==null){
            throw  new RuntimeException("classe introuvable");
        }else {
            obj.implimete();
        }

    }
}
